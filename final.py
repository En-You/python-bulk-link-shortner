import requests

# Set your Cutt.ly API key and the input/output file paths
api_key = '#'
input_file = 'C:\\Users\\ACER\\Desktop\\input.txt'
output_file = 'C:\\Users\\ACER\\Desktop\\output.txt'

# Open the input file and read the URLs
with open(input_file, 'r') as f:
    urls = f.read().splitlines()

# Loop through the URLs and shorten them using the Cutt.ly API
shortened_urls = []
for i, url in enumerate(urls):
    api_endpoint = f'https://cutt.ly/api/api.php?key={api_key}&short={url}'
    response = requests.get(api_endpoint)

    if response.status_code == 200:
        data = response.json()['url']
        shortened_url = data.get('shortLink')
        if shortened_url:
            shortened_urls.append(f'{i+1}. {shortened_url}')
        else:
            print(f'Error: Unable to shorten URL: {url}')
    else:
        print('Error: Unable to connect to the API.')

# Write the shortened URLs to the output file
if output_file:
    with open(output_file, 'w') as f:
        f.write('\n'.join(shortened_urls))
else:
    with open('output.txt', 'w') as f:
        f.write('\n'.join(shortened_urls))
